function ValidateEmail(Email)
{
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(Email) == false) 
        {
            return false;
        }

        return true;

}
function CheckName(Name)
{
	var RegularExpression = /^[a-zA-Z ]*$/;
	if(Name=="")
	{
		return false;
	}
	if (RegularExpression.test(Name) == false)
	{
		return false;
	}
	return true;
}
function CheckNumber(Name,Length)
{
	var RegularExpression = /^(\d+)$/;
	alert(Length);
	alert(Name.length);
	if ( RegularExpression.test(Name) == false )
	{
		return false;
	}
	else if ( Length == Name.length )
	{
		return true;
	}
}
function ValidateForm()
{
	
	var Flag = 1;
	var FirstName = document.Form.FirstName.value;
	if (!CheckName(FirstName))
	{
		alert("Only Letter and white spaces are allowed in Last name");
		document.getElementById('FirstName').focus();
		return false;
	}	
	var LastName = document.Form.LastName.value;
	if (!CheckName(LastName))
	{
		alert( "Only Letter and white spaces are allowed in Last name");
		return false;
	}
	var Email = document.Form.Email.value;
	if (!ValidateEmail(Email))
	{
		alert( "Invaid Email address");
		return false;
	}
	var State = document.Form.State.value;
	if (!CheckName(State))
	{
		alert( "Only Letter and white spaces are allowed in State name");
		return false;
	}
	var Country = document.Form.Country.value;
	if (!CheckName(Country))
	{
		alert( "Only Letter and white spaces are allowed in Country name");
		return false;
	}
	var City = document.Form.City.value;
	if (!CheckName(City))
	{
		alert( "Only Letter and white spaces are allowed in City name");
		return false;
	}
	var Pincode = document.Form.Pincode.value;
	if (!CheckNumber(Pincode,6))
	{
		alert("Enter an valid Pincode");
		return false;
	}
	var PhoneNumber = document.Form.PhoneNumber.value;
	if (!CheckNumber(PhoneNumber,10))
	{
		alert("Enter an valid Phone Number");
		return false;
	}
	
	return true;
}