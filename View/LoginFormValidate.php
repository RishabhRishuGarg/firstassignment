<?php
    
    require "../Core/Utility.php";
    require "../Core/Author.php";
    require "../Core/Viewer.php";
    
    $Err="";
    $Flag=1;
    $UserObj = Null;
    $UserId = $Password = $IsAuthor = "";
    function IsEmpt($Required)
    {
        $Err="";
        foreach ($Required as $Key => $Value) 
        {
            if (1 == $Value && empty($_POST[$Key]))
            {
                $GLOBALS["Flag"] = 0;
                $Err = $Err.$Key." is required ";
                break; 
            }
            else
            {
                $GLOBALS[$Key] = $_POST[$Key];
            }    
        }
        return $Err;
    }
    if ("POST" == $_SERVER["REQUEST_METHOD"])
    {
        $Required=array(
            "UserId" => 1,
            "Password" => 1,
            );
        $GLOBALS["Err"] = IsEmpt($Required);
        if(!empty($_POST["IsAuthor"]) && 1 == $_POST["IsAuthor"])
        {
            $IsAuthor = 1;
            $UserObj = new Author();
        }
        else
        {
            $IsAuthor = 0;
            $UserObj = new Viewer();
        }
    }
    if (1 == $Flag)
    {
        $StatusValue = $UserObj->Login($UserId,$Password,$IsAuthor);
        if (1 == $StatusValue)
        {
            echo "Login Successfull";
            header("Location: ./index.php");
        }
        else 
        {
            header('Location: Login.php?Err=Invalid Details! Please try again.');

        }
    }
    else
    {
        header('Location: Login.php?Err='.$Err);
    }

    
?>