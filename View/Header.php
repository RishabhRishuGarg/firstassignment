
<?php
  
  require "../Core/Utility.php";
  require "../Core/Author.php";
  require "../Core/Viewer.php";
  

?>
 
  <meta charset = "utf-8">
    <meta http-equiv = "X-UA-Compatible" content = "IE = edge">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1">
    <link rel="stylesheet" type="text/css" href="./Css/Header.css">
    <title> Blogging </title>
    <link rel = "stylesheet" href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    
  </script>

  <!-- Latest compiled JavaScript -->
  <!-- <script src = "https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
  <script type = "text/javascript" src = "./Js/Header.js"></script>
    
  <!-- <div class = "row">
    <div class = "col-sm-10"> -->
        <nav class="navbar navbar-inverse navbar-fixed-top  ">
            <div class="container-fluid">        
                <div class="navbar-header">
                    <a class="navbar-brand" href="./index.php">Blogging</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="./index.php">Home</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                <?php
                    //$_SESSION["UserId"] = "1";
                    if (1 == IsLogIn())
                    {
                        echo "
                            <li><a href=./Profile.php><span class='glyphicon glyphicon-user'></span> Profile</a></li>
                            <li><a href=./Logout.php><span class='glyphicon glyphicon-log-out'></span> Logout</a></li>
                            ";

                    }
                    else
                    {

                        echo "
                            <li><a href=./SignupView.php><span class='glyphicon glyphicon-user'></span> Sign Up</a></li>
                            <li><a href=./Login.php><span class='glyphicon glyphicon-log-in'></span> Login</a></li>
                              ";
                    }
                ?>
                </ul>
            </div>
        </nav>
      
      </div>
        
    </div>      
  </div>
    