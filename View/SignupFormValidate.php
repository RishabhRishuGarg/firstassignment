<?php
    require "../Core/Author.php";
    require "../Core/Viewer.php";
    
    $Err="";
    $Flag=1;
    $UserObj = Null;
    $UserId = $Password = $IsAuthor = "";
    function IsEmpty($Required)
    {
        
        foreach ($Required as $Key => $Value) 
        {
            if (1 == $Value && empty($_POST[$Key]))
            {
                $GLOBALS["Flag"] = 0;
                $GLOBALS["Err"] = $GLOBALS["Err"].$Key." is required ";
                break; 
            }
            else
            {
                $GLOBALS[$Key] = $_POST[$Key];
            }    
        }
        
    }
    if ("POST" == $_SERVER["REQUEST_METHOD"])
    {
        $Required=array(
            "FirstName" => 1,
            "LastName" => 1,
            "Email" => 1,
            "Password" => 1,
            "NickName" => 0,
            "Address" => 1,
            "Country" => 1,
            "State" => 1,
            "City" => 1,
            "Pincode" => 1,
            "PhoneNumber" => 1,
            "RePassword" => 1
            );
        IsEmpty($Required);
        if(1 == $Flag)
        {    
            if (!($_POST["Password"] == $_POST["RePassword"]))
            {
                $Flag = 0;
                $Err = "Password Does Not Match. Please try again";       
            }
           
            else if (
                !preg_match("/^[a-zA-Z ]*$/",$_POST["FirstName"]) || 
                !preg_match("/^[a-zA-Z ]*$/",$_POST["LastName"])
            ) 
            {
                $Flag = 0;
                $Err = " Only letters and white space allowed in Name"; 
            }
            else if (!filter_var($_POST["Email"], FILTER_VALIDATE_EMAIL)) 
            {
                $Flag = 0;
                $Err = "Invalid email format"; 
            }
            else if (!empty($_POST["IsAuthor"]) && 1 == $_POST["IsAuthor"])
            {
                $IsAuthor = 1;
                $UserObj = new Author();
            }
            else
            {
                $IsAuthor = 0;
                $UserObj = new Viewer();
            }
        }
    }
    if (1 == $Flag)
    {
        $StatusValue = $UserObj->Signup(
            $_POST["FirstName"],
            $_POST["LastName"],
            $_POST["Email"],
            password_hash($_POST["Password"], PASSWORD_BCRYPT, array('cost' => 12)),
            $_POST["NickName"],
            $IsAuthor,
            $_POST["Address"],
            $_POST["Country"],
            $_POST["State"],
            $_POST["City"],
            $_POST["Pincode"],
            $_POST["PhoneNumber"]
        );
        if (0 < $StatusValue)
        {
            header("Location: ./Login.php?SignupSucess=1&Id=".$StatusValue);
        }
        else if (-2 == $StatusValue)
        {
            header('Location: SignupView.php?Err=Email already exist try with another email!');
        }
        else if (-3 == $StatusValue)
        {
            header('Location: SignupView.php?Err=Phone Number Already Exists Try another Phone Number');
        }
    }
        
    else
    {
        header('Location: SignupView.php?Err='.$Err);
    }

    
?>