<!DOCTYPE html>
<html>
<head>
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="./Css/Profile.css">
    <?php

        require "Header.php";
        require "ProfileHandler.php";
    ?>
</head>
<body >
    <?php 

        if(!empty($_GET["Err"]))
        {
            echo "<h3 style='color:Red;'> ".$_GET["Err"]. "</h3>";
        }
    ?>
    <div class = "col-sm-4">
        <div class="Profile">
          <table class = "Profile">
            <tbody>
                <tr>
                    <th scope="row">*</th>
                    <td><strong>Name</strong></td>
                    <td><?php echo $Data["Details"][0]["FirstName"]." ".$Data["Details"][0]["LastName"];?></td>
                </tr>
                <tr>
                    <th scope="row">*</th>
                    <td><strong>Email</strong></td>
                    <td><?php echo $Data["Details"][0]["Email"];?></td>
                </tr>
                <tr>
                    <th scope="row">*</th>
                    <td><strong>Nick Name</strong></td>
                    <td><?php echo $Data["Details"][0]["NickName"];?></td>
                </tr>
                <tr>
                    <th scope="row">*</th>
                    <td><strong>Address</strong></td>
                    <td>
                        <?php 

                            echo $Data["Details"][0]["Address"].
                            ", ".$Data["Details"][0]["City"]." (".$Data["Details"][0]["Pincode"].
                            "), ".$Data["Details"][0]["State"].
                            ", ".$Data["Details"][0]["Country"] ;
                        ?>    
                    </td>
                </tr>
                <tr>
                    <th scope="row">*</th>
                    <td><strong>Phone Number</strong></td>
                    <td><?php echo $Data["Details"][0]["PhoneNumber"];?></td>
                </tr>
            </tbody> 
          </table>
        </div>
    </div>
    </br>
    </br></br></br></br></br></br>
    <div class="container">
    <?php
        if (1 == ($Data["Details"][0]["IsAuthor"]))
        {
            echo " <button onclick='AddBlog()'> Add Blog </button>";
            if (!empty($Data["Posts"]))
            {
                echo "<h3>Blogs published by you</h3>";
            }
            echo "<div class='container row'>";
            foreach ($Data["Posts"] as $Artical => $ArticalData) 
            {
          
                echo "     
                    <div class='col-sm-4'>
                        <h3 class='card-title'>".$ArticalData['Title']."</h3>

                        <div class='card' style='width:400px'>
                        
                            <div class='card-body'>
                                <p class='card-text' style='padding:20px;'>".substr($ArticalData['Content'],0,200)."</p>
                                <a href='./index.php?Id=". $ArticalData["Id"] ."' class='btn btn-primary'>Full Artical</a>

                            </div>
                            <div class='card-footer text-muted'>
                                  Posted on ".explode(" ",$ArticalData['Date'])[0]." by
                                  ".$UserObj->GetAuthor($ArticalData['UserId'])."
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class='col-sm-2'>
                        <form class='form-group' method='POST' action='./DeletePost.php' >
                            <input type='hidden' id='custId' name='Id' value=".$ArticalData["Id"].">
                            <button type='submit'> 
                                <img src='./Images/Profile/BlogDelete.png' alt='Delete Post' height='20' width='20'>
                            </button>
                        </form>
                        <form class='form-group' method='POST' action='./ModifyView.php'>
                            <input type='hidden' id='custId' name='Id' value=".$ArticalData["Id"].">
                            <button type='submit' style='float:center'> 
                                <img src='./Images/Profile/Modify.jpeg' alt='Modify Post' height='20' width='20'>
                            </button>
                        </form>
                    </div>
                    
                    ";
            }
            echo "</div>";
      }
    ?>
    </div>
</body>
</html>