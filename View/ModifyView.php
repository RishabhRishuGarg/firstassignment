<!DOCTYPE html>
<html>
<head>
	<title> Modify Blog</title>
	<?php 
		require 'Header.php';

	?>

</head>
<body>

	<?php
	if ( !IsLogin())
        {
            header("Location:./Login.php");
        }
    else
    {
		require 'Modify.php';
    }

	?>

</body>
</html>