<?php
	require "../Core/Author.php";
    require "../Core/Viewer.php";
    
    $Err="";
    $Flag=1;
    $UserObj = new Author();
    $Title = $Content = "";
    
    function IsEmpty($Required)
    {
        
        foreach ($Required as $Key => $Value) 
        {
            if (1 == $Value && empty($_POST[$Key]))
            {
                $GLOBALS["Flag"] = 0;
                $GLOBALS["Err"] = $GLOBALS["Err"].$Key." is required ";
                break; 
            }
            else
            {
                $GLOBALS[$Key] = $_POST[$Key];
            }    
        }
        
    }
    if ("POST" == $_SERVER["REQUEST_METHOD"])
    {
        $Required=array(
            "Title" => 1,
            "Content" => 1,
            );
        IsEmpty($Required);

    }
    if (1 == $Flag)
    {
        $Content = filter_var($_POST["Content"], FILTER_SANITIZE_STRING);
        $Title = filter_var($_POST["Title"], FILTER_SANITIZE_STRING);
        if ($UserObj->Write($Title, $Content) > 0)
        {
            header("Location: ./Profile.php");
        }
        else
        {
            header("Location:./AddBlog.php?Err=Unable to Add blog try again");
        }
    }
    else
    {
        header('Location: ./AddBlog.php?Err='.$UserIdErr.' '.$Err);
    }




?>