<!DOCTYPE html>
<html>
<head>
    <title> Add new Blog </title>
    <?php
        require "Header.php";
        
        if ( !IsLogin())
        {
            header("Location:./Login.php");
        }
    ?>
</head>
<body>
    <form action   = "./AddBlogOperation.php" method = "POST" >
   <?php
    if (!empty($_GET["Err"]) && strlen($_GET["Err"]) > 3)
       echo "      
            <div class='alert alert-danger fade in' style='display: inline-block;'>
                <strong>Error!</strong>" 
                .$_GET["Err"].  
            "</div>"
    ?> 
    <div class = "form-inline" >
        <div class = "form-group">
            <label for  = "Blog Title">Blog Title:</label>
            <input type = "text" class = "form-control" name = "Title" style = "width: 78%;" placeholder="Enter title of blog"> 
        </div>
    </div>
    <div class="form-group">
    </div>
    

    <div class = "form-group">
        <label for  = "Content">Content:</label>
        <textarea class = "form-control" name = "Content" style = "width: 20%;"></textarea> 
    </div>

    <div>
        <input  class = "btn btn-info" type = "submit" value = "Add Blog" align = "right" >
    </div>

</form>


</body>
</html>