<?php  
  
interface BasicOperations{ 
  
    public function Write($Title,$Content); 
    public function RemovePost($Id); 
  
} 
interface Display{

    public function Show();
    public function Search($key,$FileName);
}