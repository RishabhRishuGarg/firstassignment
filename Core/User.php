<?php

/**
 * 
 */
require "DatabaseHandling.php";
class User 
{
    protected $DatabaseOperation;
    protected $UserId = "";
    function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) 
        {
            session_start();
        }
        if (isset($_SESSION['UserId']))
        {
            $this->UserId = $_SESSION["UserId"];
        }
        else
        {
            $_SESSION["UserId"] = "";
        }
        $this->DatabaseOperation = new DatabaseHandling();
    }
    protected function CheckStatus($Status)
    {
        if ($Status < 0)
        {
            return 0;
        }
        else
        {
            return $Status;
        }

    }
    public static function Factory()
    {
            return new User();
    }

    public function GetAuthor($AuthorId)
    {
        $AuthorData = $this->Search(array("Id" => $AuthorId),"User");
        if(empty($AuthorData))
        {
            return 0;
        }
        else
        {
            return $AuthorData[0]["FirstName"]." ".$AuthorData[0]["LastName"];
        }
    }
     /*
    Inputs
        1. Attributes (Assosiative Array) - List of attributes to search in Database.
        2. TableName (String)- In which table the search operation has to made.
    
    This function passes the inputs to method which is written in different class named DatabaseHandling


    Output:- return and assosiative array if found else 0
    */
    public function Search($Attributes,$TableName)
    {   
        $Results = $this->DatabaseOperation->Search($Attributes, $TableName);
        if (empty($Results))
        {
            return 0;
        }
        else
        {
            return $Results;
        } 
    }

    /*
    Inputs 
        1. UserId (String) :- UserId of User who want to login
        2. Password (String):- Password entered by user.
    
    This function check for password and stores its informaion for later use
    
    Output:
    At successful login it will return 1
    on unsuccessfull login it will return 0*/
    
    public function Login($UserId,$Password,$IsAuthor)
    {
        $Value = $this->Search(array("Id" => $UserId),"User")[0];
        if (0 == $Value)
        {
            return 3;
        }
        if (!($Value["IsAuthor"] == $IsAuthor))
            if (1 == $IsAuthor)
            {
                return 2;
            }
            else
            {
                return 3;
            }

        else if ( password_verify($Password, $this->DatabaseOperation->GetPassword($UserId)) )
        {
            $_SESSION["UserId"] = $UserId;
            return 1;
        }
        else
        {
            return 4;
        }


    }
    public function GetLatest()
    {
        $LatestPosts = $this->DatabaseOperation->GetLatestBlogs();
        return $LatestPosts;
    }

    public function GetByMonth($Month,$Year)
    {

        $GetMonthly = $this->DatabaseOperation->GetMonthlyBlogs($Month,$Year); 
        return $GetMonthly;
    }
    public function Signup(
        $FirstName,
        $LastName,
        $Email,
        $Password,
        $NickName,
        $IsAuthor,
        $Address,
        $Country,
        $State,
        $City,
        $Pincode,
        $PhoneNumber
    )

    {
        if (1 == $this->DatabaseOperation->IsExist(array("Email" => $Email),"User") )
        {
            return -2;
        }
        if (1 == $this->DatabaseOperation->IsExist(array("PhoneNumber" => $PhoneNumber),"User"))
        {
            return -3;
        }
        
        $NewId =  $this->DatabaseOperation->AddUser(
            $FirstName,
            $LastName,
            $Email,
            $Password,
            $NickName,
            $IsAuthor,
            $Address,
            $Country,
            $State,
            $City,
            $Pincode,
            $PhoneNumber
        );
        return $NewId;
    }

}




