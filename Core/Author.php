<?php
    
    require 'GetSetInterface.php';
    require 'User.php';
    // Author class implements the functions that a author can do.
    class Author extends User implements Display,BasicOperations 
    {
        
        private $FileName = "Post.txt";
        
        /*
        
            This  function is responsible of submitting the request to get the Author data from database

            Output
        */
        public function Show()
        {
            $DisplayData["Posts"] = $this->DatabaseOperation->Search(array("UserId" => $this->UserId),"Post");
            if (0 == $this->CheckStatus($DisplayData["Posts"]))
            {
                return 0;
            }
            $DisplayData["Details"] = $this->DatabaseOperation->Search(array("Id" => $this->UserId), "User");
            if (0 == $this->CheckStatus($DisplayData["Details"]))
            {
                return 0;
            }
            return $DisplayData;  
        }

        //This function takes associative array as input , Format it and pass it to store into database
        private function CheckAvailable($Attribute,$UserData)
        {
            if (empty($UserData[$Attribute]))
            {
                die("ID is required");
            }
            else
            {
                return $UserData[$Attribute];
            }

        }

        //This function take id and Title as input and store the info in Database
        public function Write($Title,$Content) 
        {
            
            return $this->CheckStatus(
                $this->DatabaseOperation->WritePost(
                    $this->UserId, 
                    addslashes($Title), 
                    addslashes($Content)
                )
            );
            
        }

        public function ModifyBlog($PostId, $Content)
        {
            $Result = $this->DatabaseOperation->Search(array("Id" => $PostId),"Post");
            if ($Result < 0 || empty($Result))
            {
                return -1;
            }  
            if ($Result[0]["UserId"] == $this->UserId) 
            {   
                $this->DatabaseOperation->ModifyPost($PostId, addslashes($Content));
                return 1;
            }
            else
            {
                return -1;
            }

        }

        //This function remove a blog key from the database
        //Not F
        public function RemovePost($Id)
        {
            return $this->DatabaseOperation->DeleteColumnById($Id, "Post");
        }
    }