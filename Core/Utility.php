<?php
function IsLogin()
{
    if (session_status() == PHP_SESSION_NONE) 
    {
        session_start();
    }
    if (array_key_exists("UserId", $_SESSION) && !"" == $_SESSION["UserId"])
    {
        return 1;
    }
    else
    {
        return 0;
    }
}
function GetUserObject()
{
    $UserId = $_SESSION["UserId"];
    $UserObj = Null;
    $DatabaseObj = new DatabaseHandling();
    $IsAuthor = $DatabaseObj->Search(array("Id" => $UserId), "User")[0]["IsAuthor"];
    if(1 == $IsAuthor)
    {
        $UserObj = new Author();
    }
    else
    {
        $UserObj = new Viewer();
    }
    return $UserObj;
}
function IsEmpty($Required)
{
    $Err="";
    foreach ($Required as $Key => $Value) 
    {
        if (1 == $Value && empty($_POST[$Key]))
        {
            $GLOBALS["Flag"] = 0;
            $Err = $Err.$Key." is required ";
            break; 
        }
        else
        {
            $GLOBALS[$Key] = $_POST[$Key];
        }    
    }
    return $Err;
}