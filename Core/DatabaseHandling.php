<?php

/**

Some error codes that are generated when any error occured while execution of 
queries or somewhere else.

Error Codes

-1 :    Error in Query 
-2 :    Query is not executes successfully

 1 :    Query executed successfully


 * 
 */
class DatabaseHandling 
{
    private $Servername = "localhost";
    private $Username = "root";
    private $Password = "mindfire";
    private $Conn;
    private $UserTable = "User";
    private $PostTable = "Post";

    /*
    This constructor is setting up the connection with database using PDO
    */
    function __construct()
    {
        try 
        {
            $this->Conn = new PDO("mysql:host=$this->Servername;dbname=BlogProject",
                            $this->Username,
                            $this->Password
                            );

            // set the PDO error mode to exception

            $this->Conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } 
        catch(PDOException $e) 
        {
                echo "Connection failed: " . $e->getMessage();
        }
    }
    /*
        This function is creating the User table according to schema 
    */
    public function CreateUserTable()
    {
        $Query = "
            CREATE TABLE User( 
            Id int AUTO_INCREMENT,
            FirstName varchar(50) NOT NULL,
            LastName varchar(50) NOT NULL,
            Email varchar(255) NOT NULL UNIQUE,
            Password varchar(16) NOT NULL,
            NickName varchar(100) ,
            IsAuthor BOOL NOT NULL DEFAULT false,
            Address varchar(1000) NOT NULL,
            Country varchar(20) NOT NULL,
            State varchar(20) NOT NULL,
            City varchar(20) NOT NULL,
            Pincode varchar(6) NOT NULL,
            PhoneNumber varchar(13) NOT NULL UNIQUE,
            PRIMARY KEY(Id)
            );
        ";

        $FormatQuery = $this->Conn->prepare($Query);
        $FormatQuery->execute();

    }

    /*
        This function is creating the User table according to schema 
    */
    public function CreatePostTable()
    {
        $Query = "
            CREATE TABLE Post( 
            Id int AUTO_INCREMENT,
            UserId int NOT NULL,
            Date TIMESTAMP NOT NULL,
            PublishedStatus BOOL NOT NULL DEFAULT false,
            Content longtext NOT NULL,
            PRIMARY KEY(Id),
            FOREIGN KEY(UserId) REFERENCES User(Id)
            );
        ";
        $FormatQuery = $this->Conn->prepare($Query);
        $FormatQuery->execute();
    }

    /*
    Input
        Query(string):- The query to be execute in MySQL

    This function executes query 
    */
    private function ExecuteQuery($Query)
    {

        if (!$FormatQuery = $this->Conn->prepare($Query))
        {
            return -1;
        }
        else
        {
            $FormatQuery->execute();
            return 1;
        }
    }
    
    /*
    Inputs:- 
        1. UserId(Int) :- Which Author is writing the Post
        2. Title(String) :- What is the title of Post
        3. Content(Text) :- content of artical

    This function inseting the data in database .

    Output:

        Return new Id generate by Database automatically
    */
    public function WritePost($UserId,$Title,$Content)
    {
        $Query = "
            insert into Post(UserId, Title, Content )
            values (".$UserId.",'".$Title."','".$Content."');
        ";
        return $this->ExecuteQuery($Query);
    }

    /*
    Input 

        Details of user collected from form

    Insert all data in database

    */
    public function AddUser(
        $FirstName,
        $LastName,
        $Email,
        $Password,
        $NickName,
        $IsAuthor,
        $Address,
        $Country,
        $State,
        $City,
        $Pincode,
        $PhoneNumber
    ){
        $Query = "
            insert into User(
                FirstName, 
                LastName, 
                Email,
                Password,
                NickName,
                IsAuthor,
                Address,
                Country,
                State,
                City,
                Pincode,
                PhoneNumber
            ) values (
                '".$FirstName."',
                '".$LastName."',
                '".$Email."',
                '".$Password."',
                '".$NickName."',
                ".$IsAuthor.",
                '".$Address."',
                '".$Country."',
                '".$State."',
                '".$City."',
                '".$Pincode."',
                '".$PhoneNumber."'
                );
            ";
        $this->ExecuteQuery($Query);
        $NewId = $this->Search(array("Email" => $Email),"User")[0]["Id"];
        return $NewId;
    }

    /*

    Input 
        Conditions(Associative Array) : Condition on which query has to generate

        Private function only this class member function can use this functaionality 

        This function generating the search query of User for condition passed as arument
    Output
        Return the generated  query
    */

    private function GenerateUserQueryForSearch($Conditions)
    {
        $Query = "select
            Id, 
            FirstName,
            LastName,
            Email,
            IsAuthor,
            NickName,
            Address,
            Country,
            State,
            City,
            Pincode,
            PhoneNumber,
            Modified 
            from User where ";
            $First = 0;
            foreach ($Conditions as $Attribute => $Value) 
            {   
                if ($First == 1)
                {
                    $Query = $Query." and"." ";
                }
                $Query = $Query.$Attribute." = '".$Value."' ";
                $First = 1;
            } 
            $Query = $Query.";";
            return $Query;
    }

    /*

    Input 
        Conditions(Associative Array) : Condition on which query has to generate

        Private function only this class member function can use this functaionality 

        This function generating the search query of Post for condition passed as arument
    Output
        Return the generated  query
    */

    private function GeneratePostQueryForSearch($Conditions)
    {
        $Query = "select
            Id, 
            UserId,
            Title,
            Content,
            Added,
            Modified
            from Post where ";
            $First=0;
            foreach ($Conditions as $Attribute => $Value) 
            {   
                if ($First == 1)
                {
                    $Query = $Query." and"." ";
                }
                $Query = $Query.$Attribute." = '".$Value."' ";
                $First = 1;
            } 
            $Query = $Query.";";
            return $Query;
    }

    /*
    
    Input
        Query(String) : Query to be execute 

    This function execute the query in MySQL

    Output:
        
        Returns the result generated by query if successfully executed
        Retunrs error codes if problem occured
    */
    private function ExecuteSearchQuery($Query)
    {
        $FormatQuery = $this->Conn->prepare($Query);
        if (! $FormatQuery )
        {
            return -1;
        }
        else
        {
            if ($FormatQuery->execute())
            {
                $FormatQuery->setFetchMode(PDO::FETCH_ASSOC);
                $Result = $FormatQuery->fetchAll();
                return $Result;
            }
            else
            {
                return -2;
            }
        }
    }
    /*

    Input 
        1. Attribute(Associative Array):- Atttibute name with its value 
        2. Table Name (String) : Table name on which existence has to check
    
    This function check wheather a attribute value exist in table or not

    Output
        If that value exist than it return 1
        else return 0
    */
    public function IsExist($Attribute, $TableName)
    {   
        $Key = array_keys($Attribute)[0];
        $Value = $Attribute[$Key];
        $Query = "select Id from ".$TableName." where ".$Key." = '".$Value."';";
        $Result = $this->ExecuteSearchQuery($Query);

        if(count($Result) > 0)
        {
            return 1;
        }
        else
        {

            return 0;
        }
    }

    /*
    
    Inputs:
        1. Conditions(Associative array):- all condition under which search operation has to be done.
        2. TableName (String):- store table name on which search operation has to made

    */
    public function Search($Conditions,$TableName)
    {
        $Query = "";
        if ("User" == $TableName)
            $Query = $this->GenerateUserQueryForSearch($Conditions);
        if ("Post" == $TableName)
            $Query = $this->GeneratePostQueryForSearch($Conditions);
        $Result = $this->ExecuteSearchQuery($Query);

        return $Result;
    }
    public function GetPassword($UserId)
    {
        $Query = "Select Password from User where Id = ".$UserId.";";
        $Result = $this->ExecuteSearchQuery($Query);
        if(count($Result)>0)
        {
            return $Result[0]["Password"];
        }
        else
        {
            return $Result;
        }
    }

    public function DeleteColumnById($Id, $TableName)
    {
        if ( empty( $this->Search(array("Id" => $Id), $TableName )))
        {
            return 0;
        }
        else
        {
            $Query = "DELETE FROM ".$TableName." WHERE Id = ".$Id.";";
            return $this->ExecuteQuery($Query);
        }
    }
    public function ModifyPost($PostId, $Content)
    {
        $Query = "update Post set 
            Content = '".$Content."' 
            where Id = ".$PostId;
            echo $Query;
        return $this->ExecuteQuery($Query);
    }
    public function GetLatestBlogs()
    {
        $Query ="select * from Post order by Modified desc";
        $Result = $this->ExecuteSearchQuery($Query);
        return $Result;


    }
    public function GetMonthlyBlogs($Month,$Year)
    {
        if (strlen($Month)<2)
        {
           $Month = "0".$Month;
        }
        $Query = 'select * from Post where Modified like "'.$Year.'-'.$Month .'%";';
        echo $Query;
        $Result = $this->ExecuteSearchQuery($Query);
        return $Result;
    }
}

