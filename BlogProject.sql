-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Post`;
CREATE TABLE `Post` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Title` varchar(500) CHARACTER SET latin1 NOT NULL,
  `Content` longtext CHARACTER SET latin1 NOT NULL,
  `Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Title` (`Title`),
  KEY `UserId` (`UserId`),
  CONSTRAINT `Post_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `User` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `Post` (`Id`, `UserId`, `Title`, `Content`, `Added`, `Modified`) VALUES
(25,	14,	'21+ Smart Ways To Get More Subscribers on YouTube in 2019',	'Facebook and Twitter have recently stepped into the video marketing game, but they still fall way short of having the kind of impact that YouTube has.\r\nPopular: How to make money on YouTube (from beginner to advanced)\r\nIf you currently have a YouTube channel, you must be wondering how you can get more YouTube subscribers, and how you can increase the reach of your YouTube videos.\r\n\r\nWith over a billion unique YouTube visitors per month, there is a large potential audience for every uploaded video. Whether itâ€™s a video of someone performing a prank or a video of a fashionistaâ€™s summerwear review, YouTube is the go-to platform for video consumption.\r\n\r\nWith a whole new lot of YouTube stars on the rise, it is definitely a platform to be considered by everyone looking for some online marketing and publicity.\r\n\r\nYouTube is a great platform for bloggers to broaden their internet reach by making a personal connection with their followers.\r\n\r\nThe possibility for a blogger to leverage this social media giant and drive huge traffic from YouTube is high. To do that, one first needs to get more subscribers.\r\n\r\nNote: This detailed guide to getting YouTube subscribers is long. Make sure you have enough time now to read it, or you can always bookmark it and read it later! \r\n\r\n20 Smart Ways To Get More YouTube Subscribers in 2019\r\nAnother year has gone by and your resolution to re-build that YouTube channel of yours has not been accomplished.\r\n\r\nIf you are thinking about revamping your YouTube channel, here are 20 smart ways to gain more YouTube subscribers in 2019.\r\n\r\nI recently had the opportunity to attend a YouTube fan-fest with a VIP invite (thanks to my wonderful acquaintances), and a little interaction with them will shed some light on how these things work out.\r\n\r\nI have another associate (a student of mine) who saw a tremendous amount of growth in his YouTube subscribers and viewers within a span of two years. Although his numbers might not be outrageous, they total well above a million views, which more than justifies his efforts.\r\n\r\nAll that said, correctly implementing these techniques will surely help you achieve your goals.\r\n\r\n1. Add Watermark to your video.\r\n\r\nThis is a nice little hack that you can use for your YouTube channel right away. Youtube let you add a watermark that could be shown on all your videos and at all the time. This adds another way for your viewers to subscribe to your channel. This is how this watermark looks like:                ',	'2018-06-06 04:47:03',	'2019-02-07 12:08:47'),
(26,	14,	'How to Make Your Future Habits Easy',	'This article is an excerpt from Atomic Habits, my New York Times bestselling book.\r\n\r\nWhile researching Atomic Habits, I came across a story that immediately struck me with its simplicity and power. It was the story of Oswald Nuckols, an IT developer from Natchez, Mississippi, and his simple strategy for making future habits easy.\r\n\r\nNuckols refers to the approach as â€œresetting the room.â€\r\n\r\nFor instance, when he finishes watching television, he places the remote back on the TV stand, arranges the pillows on the couch, and folds the blanket. When he leaves his car, he throws any trash away. Whenever he takes a shower, he wipes down the toilet while the shower is warming up. (As he notes, the â€œperfect time to clean the toilet is right before you wash yourself in the shower anyway.â€)\r\n\r\nThis might sound like he&#39;s just â€œcleaning upâ€ but there is a key insight that makes his approach different. The purpose of resetting each room is not simply to clean up after the last action, but to prepare for the next action.\r\n\r\nâ€œWhen I walk into a room everything is in its right place,â€ Nuckols wrote. â€œBecause I do this every day in every room, stuff always stays in good shape . . . People think I work hard but Iâ€™m actually really lazy. Iâ€™m just proactively lazy. It gives you so much time back.â€\r\n\r\nI have written previously about the power of the environment to shape your behavior. Resetting the room is one way to put the power back in your own hands. Let&#39;s talk about how you can use it.\r\n\r\nThe Power of Priming the Environment\r\nWhenever you organize a space for its intended purpose, you are priming it to make the next action easy. This is one of the most practical and simple ways to improve your habits.\r\n\r\nFor instance, my wife keeps a box of greeting cards that are presorted by occasionâ€”birthday, sympathy, wedding, graduation, and more. Whenever necessary, she grabs an appropriate card and sends it off. She is incredibly good at remembering to send cards because she has reduced the friction of doing so.\r\n\r\nFor years, I was the opposite. Someone would have a baby and I would think, â€œI should send a card.â€ But then weeks would pass and by the time I remembered to pick one up at the store, it was too late. The habit wasnâ€™t easy.\r\n\r\nThere are many ways to prime your environment so itâ€™s ready for immediate use. If you want to cook a healthy breakfast, place the skillet on the stove, set the cooking spray on the counter, and lay out any plates and utensils youâ€™ll need the night before. When you wake up, making breakfast will be easy.\r\n\r\nHere are some more:\r\n\r\nWant to draw more? Put your pencils, pens, notebooks, and drawing tools on top of your desk, within easy reach.\r\nWant to exercise? Set out your workout clothes, shoes, gym bag, and water bottle ahead of time.\r\nWant to improve your diet? Chop up a ton of fruits and vegetables on weekends and pack them in containers, so you have easy access to healthy, ready-to-eat options during the week.\r\nThese are simple ways to make the good habit the path of least resistance.',	'2019-02-07 05:39:57',	'2018-03-07 11:26:27'),
(29,	14,	'NewBlog',	'Blogger\r\nMyyfgbnm\r\nalert(&#39;a&#39;);',	'2019-02-07 12:11:43',	'2018-06-07 12:11:43');

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(50) CHARACTER SET latin1 NOT NULL,
  `LastName` varchar(50) CHARACTER SET latin1 NOT NULL,
  `Email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `Password` varchar(1500) CHARACTER SET latin1 NOT NULL,
  `NickName` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `IsAuthor` tinyint(1) NOT NULL DEFAULT '0',
  `Address` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `Country` varchar(20) CHARACTER SET latin1 NOT NULL,
  `State` varchar(20) CHARACTER SET latin1 NOT NULL,
  `City` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Pincode` varchar(6) CHARACTER SET latin1 NOT NULL,
  `PhoneNumber` varchar(13) CHARACTER SET latin1 NOT NULL,
  `Added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Email` (`Email`),
  UNIQUE KEY `PhoneNumber` (`PhoneNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `User` (`Id`, `FirstName`, `LastName`, `Email`, `Password`, `NickName`, `IsAuthor`, `Address`, `Country`, `State`, `City`, `Pincode`, `PhoneNumber`, `Added`, `Modified`) VALUES
(14,	'Rishabh',	'Garg',	'rishabhgarg777@gmail.com',	'$2y$12$S9mQDJpQpZqbBIg98G9JSeoPMsSXIu1hy/SBMDrTXbyrZTXruXaUC',	'Rishu',	1,	'Flat 207 Shaiyansh enclave',	'India',	'Orissa',	'Bhubaneswar',	'713209',	'7876044546',	'2019-02-06 02:41:52',	'2019-02-06 02:41:52'),
(15,	'Surya',	'Rath',	'suryakant@gmail.com',	'$2y$12$vH9BMJyjX93Kf00af77NU.2Ofn0Q594jVdobsIwt9m45PrLjogxeW',	'',	0,	'Lingipur temple',	'India',	'Orissa',	'Bhubaneswar',	'109811',	'9898989898',	'2019-02-06 02:59:12',	'2019-02-06 02:59:12'),
(16,	'Prakash',	'Mandal',	'prakashmandal@gmail.com',	'$2y$12$w5Dps0G9FqQ2ltlQ4qsPtOY8OYO1gvCOg5wOxPux5hiSVmYG8ehSy',	'kash',	1,	'Room number 315 Hall 9 NIT Durgapur',	'India',	'West Bangal',	'Durgapur',	'713209',	'9896012332',	'2019-02-06 05:15:33',	'2019-02-06 05:15:33'),
(17,	'Surya',	'Rath',	'suyakant@gmail.com',	'$2y$12$tHajywcMmUNyOTGuDI2ZuusYmZ6CPBxzAwO5IP17LOai90qgrc35O',	'',	0,	'Lingipur temple',	'India',	'Orissa',	'Bhubaneswar',	'109811',	'9898989890',	'2019-02-06 05:57:44',	'2019-02-06 05:57:44'),
(18,	'Prakash',	'Mandal',	'prakashmanda@gmail.com',	'$2y$12$gNrqi/h/2085jyCprbEj8OJLSXWl.9HTVpXujySa.owXpcBIVgnHe',	'',	0,	'Flat 207 Shaiyansh enclave',	'India',	'Orissa',	'Bhubaneswar',	'123456',	'9789789786',	'2019-02-07 11:40:10',	'2019-02-07 11:40:10');

-- 2019-02-12 10:27:09
